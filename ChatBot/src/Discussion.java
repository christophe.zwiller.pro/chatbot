/**
 * Importation de différentes classes du package Java.io de la bibliotèque standard de Java
 */

/**
 * La classe BufferedOutputStream est une classe de flux de sortie
 * qui utilise la mémoire tampon pour améliorer les performances 
 * lors de l'écriture de données dans un flux de sortie.
 */ 
import java.io.BufferedOutputStream;
/**
 * La classe BufferedReader est une classe de lecteur de caractères
 * qui utilise la mémoire tampon pour améliorer les performances lors 
 * de la lecture de caractères d'un flux d'entrée.
 */
import java.io.BufferedReader;
/**
 * La classe IOException est une exception qui est levée lorsqu'il y 
 * a une erreur d'entrée/sortie.
 */
import java.io.IOException;
/**
 * La classe InputStream est une classe abstraite qui représente un 
 * flux d'octets en entrée.
 */
import java.io.InputStream;
/**
 * La classe InputStreamReader est une classe qui permet de lire des 
 * caractères à partir d'un flux d'octets en entrée en utilisant un 
 * encodage spécifique.
 */
import java.io.InputStreamReader;
/**
 * La classe OutputStream est une classe abstraite qui représente un 
 * flux d'octets en sortie.
 */
import java.io.OutputStream;


/**
 * Classe Discussion permet d'effectuer diverses opérations en fonctions des actions de l'utilisateur
 * (ex : bye permet de terminer la discussion)
 */
public class Discussion {
    
    private GenPhrase sentenceGenerator;
    
    private BufferedReader inputFromOther;
    
    private BufferedOutputStream outputToOther;
    
    
    /**
     * Create a discussion object. The streams genASentence() {e bufferized in the constructor.
     * @param in : stream where the questions are read
     * @param out : stream where answers are written
     * @param gen : sentence generator which read the questions and create the 
     * answers
     */
    public Discussion(InputStream in, OutputStream out, GenPhrase gen ){
    
    try{

        inputFromOther = new BufferedReader(new InputStreamReader(in));
        outputToOther = new BufferedOutputStream(out);

        sentenceGenerator = gen;    
    }catch(Exception e){
        System.out.println(e.getMessage());
        
    }
    }


    /**
     * Read a sentence from the input, give it to the generator and send the 
     * generated answer to the output, until the keyword "bye" is inputed.
     */
    public void discussion(){
        
        boolean end = false;
        do{
            try{
                String quest = inputFromOther.readLine();
                if(quest.equals("bye")){
                    end = true;
                }else{
                    sentenceGenerator.loadASentence(quest); 
                    String ans = ">> " +sentenceGenerator.genASentence()+"\n";
                    //outputToOther.write(ans.getBytes());
                    System.out.println(ans);
                    // System.out.println("CCCCCCCCCCCCC");        //tests
                    //outputToOther.flush();
                    // System.out.println("DDDDDDDDDDD");

                }
            }catch(IOException e){
                System.out.print("Oups... " + e.getMessage());
                end = true;
            }
        }while(!end);
        
    }
}
