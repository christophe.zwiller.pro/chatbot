import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
public static void main(String[] args) throws IOException {
// On crée un objet Socket qui va se connecter au serveur sur le port 5000
Socket socket = new Socket("localhost", 5000);
System.out.println("Connecté au serveur !");

    // On crée un objet BufferedReader à partir de l'InputStream du serveur
    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    // On crée un objet PrintWriter à partir de l'OutputStream du serveur
    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

    // On demande à l'utilisateur de saisir un message à envoyer au serveur
    Scanner sc = new Scanner(System.in);
    System.out.println("Saisissez un message à envoyer au serveur :");
    String messageToServer = sc.nextLine();

    // On envoie le message au serveur
    out.println(messageToServer);

    // On attend la réponse du serveur
    String messageFromServer = in.readLine();
    System.out.println("Message reçu du serveur : " + messageFromServer);
}

}