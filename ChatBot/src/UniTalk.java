
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Medium version of the generator
 * Génère une phrase avec des mots aléatoire 
 * avec un point à la fin
 * @author mouchere-h
 */
public class UniTalk implements GenPhrase
{
    /**
     * the dictionary containing all our knowlegde
     */
    private List<String> dictionary;

    private int initDictionaryLenght;

    /**
     * Create the dictionary (with at least a ".")
     */
    UniTalk() {
        dictionary = new ArrayList<>(); // créer tableau "dictionnaire" qui ne contient pas de mots en double  ......Vector<>
        dictionary.add("."); // add a point to be sure to finish the sentence
    }

    /**
     * Diviser le fichier en lignes et les ajouter toutes avec loadASentence(.)
     * 
     * @param filepath the file name in working directory
     */
    @Override
    public void initFromFile(String filepath) {
        BufferedReader bReader; // Déclaration d'un lecteur de fichier

        try {

            bReader = Files.newBufferedReader(FileSystems.getDefault().getPath(filepath)); // Crée un objet Path
                                                                                           // correspondant au chemin du
                                                                                           // fichier voulu

            String line; // Déclaration d'une chaine de caractère qui va contenir chaque ligne du fichier texte choisi

            while ((line = bReader.readLine()) != null) { // tant qu'il y a une ligne, on la lit
                loadASentence(line);
            }

            
        } catch (IOException ex) {
            System.out.println("Problem in initFromFile (file not found ?" + filepath + ")" + ex.getMessage());
        }
        System.out.print(" : " + dictionary.size() + " words loaded");

        initDictionaryLenght = dictionary.size();
    }

    /**
     * Récupère une ligne du texte et ajoute les mots 
     * individuellement au dictionnaire
     * 
     * @param newQuestion
     */
    @Override
    public void loadASentence(String newQuestion) {
        List<String> words; // Création d'une liste
        words = Arrays.asList(newQuestion.split(" ")); // <<ewQuestion.split(" ")>> -> à chaque espace création d'une
                                                                // nouvelle case dans un tableau
                                                                // Retourne un objet List à partir d'un tableau

        Iterator<String> itW = words.iterator(); // Création d'un itérateur d'une liste de String
        while (itW.hasNext()) {
            dictionary.add(itW.next());
        } // à chaque next(), on avance d'une case dans le tableau

    }

    /**
     * Créer une phrase avec des mots au hasard, 
     * jusqu'à ce qu'on tombe sur un ".!?" 
     * @return
     */
    @Override
    public String genASentence() {
        String answer = "";
        Random rdmGen = new Random();
        // select only one random word

        int longueur = 0;
        do {
            answer += " " + dictionary.get(rdmGen.nextInt(dictionary.size())); // Ajoute dans la réponse un mot en plus
                                                                               // du dictionnaire
            // if the line is too long, break it with a newline \n
            if (answer.length() - longueur > 100) {
                longueur = answer.length();
                answer += "\n";
            }
            // Tant qu'il n'y pas de "! ou . ou ?"
        } while (answer.matches("[^!\\.\\?]*"));  
        return answer;
    }

    /**
     * Returns the type and the size of the dictionary and the list of all the words
     * @return res
     */
    @Override
    public String toString() {
        String res = "\nTableau initial : \n" + dictionary.subList(0, initDictionaryLenght).toString() + "\n" +
                "\nNouveaux mots ajoutés dans la base de données : ";
        
        Iterator<String> it = dictionary.listIterator(initDictionaryLenght);
        while (it.hasNext()) {
            res += it.next() + " ";
        }

        res += "\n";
        return res;
    }//Possibilité d'utiliser un StringBuilder pour optimiser l'affichage de la chaine de caractères si besoin


    /**
     * Returns the size of the knowledge.
     * Usefull for tests.
     *
     */
    public int size() {
        return dictionary.size();
    }

    /**
     * Returns true if the word is in the knowledge.
     * Usefull for tests.
     * 
     * @param w : word to find
     */
    public boolean contains(String w) {
        return dictionary.contains(w);
    }

}
