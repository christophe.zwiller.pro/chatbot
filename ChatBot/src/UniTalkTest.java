import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 * Classe permettant d'effectuer divers test tel que des test sur la taille du fichier texte ou encore sur la présence d'un mot
 * @param args
 */
public class UniTalkTest{
    
    private UniTalk unitalk = new UniTalk();
    /**
     * setup avec @Before : Permet de charger le fichier texte avant chaque test
     */
    @Before
    public void setup(){
        // explication : Sur linux le chemin absolu débute par un slash alors que sur windows c'est "lettre_du_disque: "
        //Rq : éviter les accents dans le chemin d'accès des textes car peut causer des problèmes
        String file = this.getClass().getResource("/texts/shortCleaned.txt").getFile().substring(1); // Windows
        //String file = this.getClass().getResource("/texts/shortCleaned.txt").getFile();  //linux
        unitalk.initFromFile(file);
        System.out.println(file);
    }
    /**
     * Assertion permettant de vérifier qu'une chaine de caractère est bien présente dans le fichier
     */
    @Test
    public void testContainsUnitalk() {    
        String a = "VINGT";     
        assertTrue(unitalk.contains(a));  
        
    }
    /**
     * Assertion permettant de vérifier le bon nombre de mots du fichier texte
     */
    @Test
    public void testSizeUnitalk(){
        //assertTrue(unitalk.size() == 124); // Dans le cas du fichier short
        assertTrue(unitalk.size() > 1);    //Vérification du bon chargement du fichier texte
    }
}
