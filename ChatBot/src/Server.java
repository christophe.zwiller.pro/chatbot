import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
public static void main(String[] args) throws IOException {
// On crée un objet ServerSocket qui va nous permettre d'écouter les connexions entrantes sur le port 5000
ServerSocket serverSocket = new ServerSocket(5000);
System.out.println("Le serveur est en attente de connexion...");

    // On attend qu'un client se connecte
    Socket clientSocket = serverSocket.accept();
    System.out.println("Un client s'est connecté !");

    // On crée un objet BufferedReader à partir de l'InputStream du client
    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    // On crée un objet PrintWriter à partir de l'OutputStream du client
    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

    // On attend qu'un message arrive du client
    String messageFromClient = in.readLine();
    System.out.println("Message reçu du client : " + messageFromClient);

    // On envoie une réponse au client
    out.println("Message reçu, merci !");
}

}