import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.Map;
import java.util.Hashtable;

/**
 * Complex version of the generator
 * En fonction du mot choisi par l'utilisateur
 * Effectue une proposition de 3 mots qui pourrait
 * aller à la suite du mot comme sur application mobile de message
 */
public class BigramTalk implements GenPhrase {

    private Map<String, Set<String>> dictionary; // Table de hashage avec la clé et la valeur
    private String currentSentence = "";
    /**
     * Init the knowledge of the generator from a text fil
     * 
     * @param filepath : path to the text file
     */

    BigramTalk() {
        dictionary = new Hashtable<>(); // Création de la table de hashage
    }

    @Override
    public void initFromFile(String filepath) {

        BufferedReader bReader; // Déclaration d'un lecteur de fichier

        try {
            bReader = Files.newBufferedReader(FileSystems.getDefault().getPath(filepath));

            String line;
            String prevLastWord = ""; // Variable qui récupère le dernier mot de la ligne précédente

            while ((line = bReader.readLine()) != null) { // Tant qu'il y a une ligne à traiter

            line = line.toLowerCase().trim(); // Convertit la chaîne de caractère en minuscule et enlève les espaces en trop en début et fin de ligne 
                if (!line.equals("")) {    // Si la ligne n'est pas égal à un champs vide

                    List<String> words = new Vector<>(); // Création d'une liste d'objets de type string
                    if (!prevLastWord.equals("")) // Si prevLastWord est pas vide
                    {
                        words.add(prevLastWord); // on save la valeur du dernier mot de la ligne (pour le tour suivant)
                    }
                    words.addAll(Arrays.asList(line.split(" "))); // on ajoute au tableau words tous les mots de la
                                                                        // ligne
                    

                    prevLastWord = words.get(words.size() - 1); // on lui assigne le dernier mot de la ligne
                        
                    this.loadASentence(String.join(" ", words)); // on lance loadASentence avec la chaine de
                                                                     // caractère reconstruite
                    
                }
            }

        } catch (IOException ex) {
            System.out.println("Problem in initFromFile (file not found ?" + filepath + ")" + ex.getMessage());
        }
        System.out.print(" : " + dictionary.size() + " words loaded");


    };

    /**
     * Load a new question from the other, update the knowledge and be ready
     * for the next generation
     * @param newQuestion
     */
    @Override
    public void loadASentence(String newQuestion) {
        this.currentSentence = newQuestion;
        List<String> words = Arrays.asList(newQuestion.split(" "));   // Création d'une liste avec les mots séparés par des espaces
        Iterator<String> itW = words.iterator(); // Création d'un itérateur d'une liste de String 

        String prevWordKey = "";
        
        while(itW.hasNext()){     // Tant qu'il y a du contenu dans la prochaine case de la liste de l'itérateur
            String currentWordValue = itW.next();   // Affectation du mot de la prochaine case dans currentword
            if(prevWordKey.equals("")){    // Si prevWord est égal au champ vide alors 
                prevWordKey = currentWordValue;
                continue;         // Permet d'ignorer le reste des instructions de la boucle et passer directement à la prochaine itération
            }


            Set<String> keyNextsWords = dictionary.get(prevWordKey); // Récupère dans le dictionnaire la valeur associé à la case avec la clé prevWordKey
                                                                     // mots suivants de la clé

            //exemple avec "Récupère dans le récupère dictionnaire"
            // Set<String> keyNextsWords = dictionary.get("récupère"); => keyNextsWords == [dans, dictionnaire]
            // Set<String> keyNextsWords = dictionary.get("dans"); => keyNextsWords == [le]

            if(keyNextsWords == null){ //  si pas de tableau associé à la clé (=si on a pas encore croisé le mot, qu'il est pas dans le dico) on le créé
                Set<String> values = new HashSet<>(); //créé le tableau qui contiendra tous les mots suivants de la clé
                values.add(currentWordValue); // on lui ajoute notre currentvalue actuelle vu que c'est un mot suivant de la clé
                dictionary.put(prevWordKey, values); // et on ajoute ce HashSet de valeurs dans le dico (avec une seule valeur pour le moment)
            }
            else{  // si déjà un tableau 
                keyNextsWords.add(currentWordValue); //on ajoute la valeur dans le tableau
                dictionary.replace(prevWordKey, keyNextsWords); //et on remplace l'ancien tableau de mots suivants par le nouveau (contenant en + la nouvelle valeur)
            }
            prevWordKey = currentWordValue; //pour boucler, on passe notre currentWord dans prevWord, il devient donc le mot racine du suivant
        }
    }
    

    /**
     * Create a new sentence answering the last question and using the knowledge
     * @return
     */
    @Override
    public String genASentence() {

        String answer = "";
        
        String word = this.currentSentence.toLowerCase().trim().replaceAll("^.* (.*)$","$1");  // Word contient le dernier mot entré séparé d'un espace dans la console 
        Set<String> followingWords = dictionary.get(word);   //Création d'une liste qui prend en valeur le mot associé à la clé word
        if(followingWords != null && followingWords.size() != 0){
            answer = word+" : "+String.join(", ", followingWords).replaceAll("^(.*)$", "[$1]\n");
        }
        return answer;
    }

    /**
     * @return retourne la liste des bigrams du dictionnaire
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();  // Création d'un stringBuilder
        Set<String> keys = dictionary.keySet();  // Récupère toute les clés présentes dans l'objet dictionnary
        Iterator<String> dicoIterator = keys.iterator();  // Création d'un itérateur pour parcourir les différentes clés de la table de hashage
        sb.append("\n");
        sb.append("Liste des bigrams : \n\n");


        while(dicoIterator.hasNext()){   // Tant que il y a une clé dans le dictionnaire à parcourir
            sb.append("  ");
            String keyWord = dicoIterator.next();    // Affecte dans keywords l'élément suivant de l'itérateur dicoitérator
            sb.append(keyWord + " : [ ");            // Ajoute au stringBuilder les chaines de caractères choisi
            Set<String> followingWords = dictionary.get(keyWord);
            sb.append(String.join(", ", followingWords));
            sb.append(" ] \n");
        }
        return sb.toString();
    }

    public boolean contains(String word){
        return dictionary.keySet().contains(word);
    }

    public int size(){
        return dictionary.size();
    }


}

