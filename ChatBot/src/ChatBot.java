import java.util.Scanner;

/**
 * Programme principal : Robot de discussion
 */
public class ChatBot {

    public static GenPhrase selectRobot(Scanner scanner) {
            
        System.out.println("Avec quel type de robot souhaitez vous interagir ?");
        System.out.println("1 : dumb robot");
        System.out.println("2 : medium robot");
        System.out.println("3 : complex robot\n");

        int choice_1 = 0;
        GenPhrase theGen = null; // Initialisation de theGen à null

        do {
            System.out.print("=> ");
            choice_1 = scanner.nextInt();

            switch (choice_1) {
                case 1:
                    theGen = new Dumb(); // Création du générateur de phrase Dumb
                    break;
                case 2:
                    theGen = new UniTalk(); // Création du générateur de phrase UniTalk
                    break;
                case 3:
                    theGen = new BigramTalk(); // Création du générateur de phrase BigramTalk
                    break;    
                default:
                    // Choix non valide
                    System.out.println("Choix non valide. Veuillez réessayer.");
                    break;
            }
        } while (choice_1 != 1 && choice_1 != 2 && choice_1 != 3);

        return theGen;
    }

    public static void textSelect(GenPhrase theGen, Scanner scanner) {

        System.out.println("\nQuel base de données souhaitez vous charger ?");
        System.out.println("1 : shortCleaned.txt");
        System.out.println("2 : longCleaned.txt");
        System.out.println("3 : 20000LieuesSousLesMers.txt\n");

        int choice = 0;
        long startTime = 0;

        do {
            System.out.print("=> ");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    // Chargement du fichier "shortCleaned.txt"
                    startTime = System.nanoTime();
                    System.out.print("\nshortCleaned.txt");
                    theGen.initFromFile("src/texts/shortCleaned.txt");
                    break;
                case 2:
                    // Chargement du fichier "longCleaned.txt"
                    startTime = System.nanoTime();
                    System.out.print("\nlongCleaned.txt");
                    theGen.initFromFile("src/texts/longCleaned.txt");
                    break;
                
                case 3:
                    // Chargement du fichier "longCleaned.txt"
                    startTime = System.nanoTime();
                    System.out.print("\n20000LieuesSousLesMers.txt");
                    theGen.initFromFile("src/texts/20000LieuesSousLesMers.txt");
                    break;    
                default:
                    // Choix non valide
                    System.out.println("Choix non valide. Veuillez réessayer.\n");
                    break;
            }
        } while (choice != 1 && choice != 2 && choice != 3);
 
        long estimatedTime = System.nanoTime() - startTime;                   // estimatedTime -> contient l'heure actuelle - l'heure avant le chargement du fichier texte
        System.out.println(" in " + estimatedTime/1000000.0 + "ms\n");        // Affiche le tps de chargement du fichier texte avec une conversion de nano sec vers ms
    }



    

    /**
     * Main
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("\n-------------------------------------------");
        System.out.println("        Initialisation des paramètres");
        System.out.println("-------------------------------------------");

        Scanner scanner = new Scanner(System.in);
        // Choix du robot
        GenPhrase theGen = selectRobot(scanner);

        // Choix du texte
        textSelect(theGen,scanner);

        
        
        System.out.println("------------------------------------------");
        System.out.println("          Robot de Discussion");
        System.out.println("------------------------------------------");

        System.out.print("\nVeuillez entrer votre mot ou phrase : ");
        

        Discussion talk = new Discussion(System.in, System.out, theGen);   // Création de la discussion talk

        talk.discussion();  

        System.out.println("=> Fin de dicussion avec le robot");
        System.out.println(theGen.toString());  // Affiche le toString dans la console
        scanner.close();
        
    }
    
}
