
/**
 * Sentence generator interface
 */
public interface GenPhrase {
    
    /**
     * Init the knowledge of the generator from a text fil
     * @param filepath : path to the text file
     */
    public void initFromFile(String filepath );
    
    /**
     * Load a new question from the other, update the knowledge and be ready 
     * for the next generation
     * @param newQuestion 
     */
    public void loadASentence(String newQuestion);
    
    /**
     * Create a new sentence answering the last question and using the knowledge
     * @return 
     */
    public String genASentence();
    
    /**
     * 
     * @return return a text version of the knowledge
     */
    @Override
    public String toString();
    
}
